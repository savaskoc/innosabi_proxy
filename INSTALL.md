# Proxy Assignment
I made a small project with Symfony 4.4 (LTS). Minimal version used instead of full version.
## Installation
Since this is a composer project, a simple `composer install` is enough to install this project.
## Settings
You can provide `API_URL`, `API_USERNAME` and `API_PASSWORD` environment variables. For security, `API_PASSWORD` assigned within [secrets](https://symfony.com/doc/current/configuration/secrets.html).
## Tests
After install, you can run tests with `php bin/phpunit` command.
## Notes
* You can specify page with ?page= parameter.
* If you do not specify any include, proxy will not filter keys.
