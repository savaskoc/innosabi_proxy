<?php

namespace App\Tests\Service\TransformService;

use App\Service\TransformService\MapTransformService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class MapTransformServiceTest extends KernelTestCase
{
    protected $transformService;

    protected function setUp()
    {
        static::bootKernel();

        $this->transformService = static::$container->get(MapTransformService::class);
    }

    /**
     * @dataProvider provider
     */
    public function testOnly($array, $keys)
    {
        $transformed = $this->transformService->only($array, $keys);
        foreach ($transformed as $index => $item) {
            $this->assertSame($keys, array_keys($item));
        }
    }

    public function provider()
    {
        return [
            [
                [
                    [
                        'name' => 'Solution Scouting',
                        'description' => 'Help us solve challenges in our development and production.',
                    ],
                    [
                        'name' => 'Solution Scouting',
                        'description' => 'Help us solve challenges in our development and production.',
                    ],
                ],
                ['name'],
            ],
        ];
    }
}
