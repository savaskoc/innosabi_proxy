<?php

namespace App\Tests\Service\ProxyService;

use App\Service\ProxyService\HttpClientProxyService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class HttpClientProxyServiceTest extends KernelTestCase
{
    protected $httpClient;

    protected function setUp()
    {
        static::bootKernel();

        $this->httpClient = static::$container->get(HttpClientProxyService::class);
    }

    public function testGetProjects()
    {
        $projects = $this->httpClient->getProjects();

        $this->assertIsArray($projects);
        $this->assertNotEmpty($projects);
    }
}
