<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProxyControllerTest extends WebTestCase
{
    /**
     * @dataProvider provider
     */
    public function testInnosabiProjects(array $parameters, array $expectedKeys)
    {
        $client = static::createClient();
        $client->request('GET', '/innosabi-projects', $parameters);

        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertFalse($response->isEmpty());

        $json = json_decode($response->getContent(), true);
        $this->assertEquals($parameters['page'] ?? 0, $json['page']);
        if ($expectedKeys) {
            foreach ($json['data'] as $item) {
                $this->assertSame($expectedKeys, array_keys($item));
            }
        }
    }

    public function provider()
    {
        return [
            [
                ['page' => 1],
                [],
            ],
            [
                ['include' => 'name,description'],
                ['name', 'description'],
            ],
            [
                ['page' => 2, 'include' => 'name'],
                ['name'],
            ],
        ];
    }
}
