<?php

namespace App\Controller;

use App\Service\ProxyService\ProxyServiceInterface;
use App\Service\TransformService\TransformServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProxyController extends AbstractController
{
    /**
     * @Route("innosabi-projects", methods={"GET"})
     */
    public function innosabiProjects(Request $request, ProxyServiceInterface $proxyService,
                                     TransformServiceInterface $transformService)
    {
        $projects = $proxyService->getProjects($request->get('page'));
        if ($projects && $include = $request->query->get('include')) {
            $projects['data'] = $transformService->only($projects['data'], explode(',', $include));
        }
        return $this->json($projects);
    }
}
