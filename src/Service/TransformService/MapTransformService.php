<?php

namespace App\Service\TransformService;

use Psr\Log\LoggerInterface;

class MapTransformService implements TransformServiceInterface
{
    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function only(array $array, array $keys): array
    {
        $this->logger->debug('Only fields', compact('keys'));

        return array_map(function ($item) use ($keys) {
            return array_filter($item, function ($key) use ($keys) {
                return in_array($key, $keys);
            }, ARRAY_FILTER_USE_KEY);
        }, $array);
    }
}
