<?php

namespace App\Service\TransformService;

interface TransformServiceInterface
{
    public function only(array $array, array $keys): array;
}
