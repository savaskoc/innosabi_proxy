<?php

namespace App\Service\ProxyService;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Contracts\HttpClient\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class HttpClientProxyService implements ProxyServiceInterface
{
    protected $httpClient;
    protected $kernel;
    protected $logger;

    public function __construct(HttpClientInterface $innosabiClient, KernelInterface $kernel, LoggerInterface $logger)
    {
        $this->httpClient = $innosabiClient;
        $this->kernel = $kernel;
        $this->logger = $logger;
    }

    public function getProjects(?int $page = null): array
    {
        try {
            $page = $page ?? 0;
            return $this->httpClient->request('GET', 'project/filter', ['query' => compact('page')])->toArray();
        } catch (ExceptionInterface $exception) {
            $this->logger->error('Projects could not be fetched', compact('exception'));

            if ($this->kernel->isDebug()) {
                throw $exception;
            }
            return [];
        }
    }
}
