<?php

namespace App\Service\ProxyService;

interface ProxyServiceInterface
{
    public function getProjects(?int $page = null): array;
}
